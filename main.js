/*****
/* Main application entry point.
/*****/

var app = require('./server/app');

app.run();

console.log("App listening on port: " + app.port);
