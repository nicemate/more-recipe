/*****
/* Root directory routes are defined in this file, with their associated controllers.
/*****/

// Initialize Express
var express = require('express');

// Import recipe router
var recipeRouter = require('./recipe');

// Create new Express router.
var router = express.Router();

// Register Recipe Router
router.use('/recipes', recipeRouter);

module.exports = router;
