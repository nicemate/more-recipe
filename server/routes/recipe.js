/*****
/* /recipe directory routes are defined in this file, with their associated controller functions.
/*****/

var express = require('express');

// Create new Express router.
var router = express.Router();

// Require the route request handler.
var recipeController = require('../controllers/recipe');

var controller = new recipeController();


/*****
/* RECIPE ROUTES
/*****/

// GET /recipes: Get all recipess.
router.get('/', controller.getRecipes);

module.exports = router;
