/*****
/* The Controller is an essential component of
/* this application as it interacts with the user requests and the data layer (database).
/*****/
var recipeModel = require('../models/recipe');
var model = new recipeModel();

class RecipeController {
    /***
	/* Class constructor.
	/***/
	constructor() {

	}

    /***
	/* Method to get all recipes.
	/***/
    getRecipes (request, result) {
        result.status(200).send({
            status: 'Success',
            message: 'All Recipes',
            recipes: model.getRecipes()
        });
    };
}

module.exports = RecipeController;