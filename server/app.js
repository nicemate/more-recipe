var express = require('express');
var logger = require('morgan');
var bodyParser = require('body-parser');
var http = require('http');

var index = require('./routes/index');

// Initialize the express app
var app = express();

// Port parameter
var port = 3232;

function run() {
	// Enable developer logging in server console.
	app.use(logger('dev'));

	// Make the app take the body and parse any JSON.
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: false }));

	// Make app use the specified routes from the ../routes directory.
    app.use('/api', index);

	// App listens on port specified.
	var httpServer = http.createServer(app);
	httpServer.listen(port);
}

module.exports = {
	run: run,
	port: port
};