/***
/* Recipe class is used to maintain Recipe objects.
/***/
class RecipeModel {
    /***
    /* Class constructor assigns values to recipe object.
    /***/
    constructor() {
      this.recipes = [
        {
          id: 1,
          title: 'Cheese-burger',
          caption: 'Lunch #hamont',
          upvote: 0,
          downvote: 0,
          description: 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur',
          image: 'https://scontent.cdninstagram.com/hphotos-xap1/t51.2885-15/e35/12552326_495932673919321_1443393332_n.jpg'
        },
        {
          id: 2,
          title: 'Cheese-burger',
          caption: 'Lunch #hamont',
          upvote: 0,
          downvote: 0,
          description: 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur',
          image: 'https://scontent.cdninstagram.com/hphotos-xap1/t51.2885-15/e35/12552326_495932673919321_1443393332_n.jpg'
        }
      ];
    }

    /***
    /* Returns all recipes in DB.
    /***/
    getRecipes () {
        return this.recipes;
    }
}


module.exports = RecipeModel;